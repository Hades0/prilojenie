import json

from PyQt5.QtWidgets import QWidget, QFileDialog, QTableWidgetItem, QMessageBox
from ui_py import ui_viewing_json_window

class ViewingCsvWindow(QWidget, ui_viewing_json_window.Ui_Form):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton_loadCSV.clicked.connect(self.btn_viewingcsv_click)

    def btn_viewingcsv_click(self):
        fd = QFileDialog.getOpenFileName(self,
                                         f'{self.windowTitle()} [открыть]',
                                         '',
                                         'JSON (*.json);;txt (*.txt);;All (*.*)')
        if fd[0]:
            with open(fd[0], 'r', encoding='utf-8') as f:
                json_data = f.read()
            print(json_data)
