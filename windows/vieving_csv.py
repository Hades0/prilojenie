from PyQt5.QtWidgets import QWidget, QFileDialog, QTableWidgetItem, QMessageBox
from ui_py import ui_viewing_csv_window
import csv


class ViewingCsvWindow(QWidget, ui_viewing_csv_window.Ui_Form):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.pushButton_loadCSV.clicked.connect(self.btn_viewingcsv_click)

    def btn_viewingcsv_click(self):
        dlmtr = self.lineEdit.text()
        if dlmtr:
            res_dlg = QFileDialog.getOpenFileName(self,
                                                  self.windowTitle(),
                                                  filter='CSV (*.csv);;txt (*.txt);;All (*.*) ')
            if res_dlg[0]:
                try:
                    with open(res_dlg[0], 'r', encoding='utf-8') as f:
                        rd = csv.DictReader(f, delimiter=';')
                        self.tableWidget.clear()
                        headers = rd.fieldnames
                        self.tableWidget.setColumnCount(len(headers))
                        self.tableWidget.setHorizontalHeaderLabels(headers)
                        for i, row in enumerate(rd):
                            self.tableWidget.setRowCount(i + 1)
                            for j, (key, value) in enumerate(row.items()):
                                self.tableWidget.setItem(i, j, QTableWidgetItem(value))
                except Exception as e:
                    QMessageBox.warning(self,
                                        self.windowTitle(),
                                        f'Выбранный файл имеет неподдерживаемый формат!\n{e.args}')
        else:
            QMessageBox.warning(self,
                                self.windowTitle(),
                                'Не указан разделитель!')
