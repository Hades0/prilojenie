from PyQt5.QtWidgets import QApplication
from windows import window_main

if __name__ == '__main__':
    app = QApplication([])
    wnd = window_main.MainWindow()
    wnd.show()
    app.exec_()
